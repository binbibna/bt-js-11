var DSSV = "DSSV";
var dssv = [];
//lấy dữ liệu từ localStorage
// object lấy từ localStorage sẽ bị mất các key chứa function ( method )
var dssvJson = localStorage.getItem("DSSV");

if(dssvJson != null){
  var svArr = JSON.parse(dssvJson);
  dssv = svArr.map(function (item){
    return new SinhVien(
      item.ma, 
      item.ten, 
      item.matKhau, 
      item.diemToan, 
      item.diemLy, 
      item.diemHoa, 
      item.email
      );
  });
  // convert array chứa object ko có key tinhDTB() thành array chứa object có key tinhDTB()
  rederDSSV(dssv);
}


//thêm sinh viên
function themSinhVien(){
  var sv = layThongTinTuForm();

    dssv.push(sv);
  // lưu vào localStorage (không bị mất dữ liệu khi load trang)


  //converr array dssv thành json
    var dssvJson = JSON.stringify(dssv);
  // lưu data json vào localStorage
    localStorage.setItem("DSSV", dssvJson);

  // render dssv ra table
    rederDSSV(dssv);
}




function xoaSinhVien(idSv){
        var viTri =timKiemViTri(idSv,dssv);

        if(viTri != -1){
          dssv.splice(viTri, 1);
          console.log(xoaSinhVien, dssv.length);
          rederDSSV(dssv);
        }
        var dssvJson = JSON.stringify(dssv);
  // lưu data json vào localStorage
  localStorage.setItem("DSSV", dssvJson);

  // render dssv ra table
  rederDSSV(dssv);
}


// sửa sinh viên

function suaSinhVien(idSv){
  var viTri = timKiemViTri(idSv, dssv);
  if(viTri == -1){
    return;
  }
  var sv = dssv[viTri];
  console.log(sv);
  showThongTinLenForm(sv);
}


function capNhatSinhVien(){
  // lấy thông tin sau khi user uppdate
  var sv = layThongTinTuForm();
  // update dữ liệu mới thay dữ liệu cũ

  var viTri = timKiemViTri(sv.ma, dssv);
  if(viTri != -1){
    dssv[viTri] = sv;
    rederDSSV(dssv);
  };


  
  var dssvJson = JSON.stringify(dssv);
  // lưu data json vào localStorage
  localStorage.setItem("DSSV", dssvJson);

  // render dssv ra table
  rederDSSV(dssv);
}
