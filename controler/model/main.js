function SinhVien(
    _maSV,
    _tenSV,
    _matKhau,
    _diemToan,
    _diemLy,
    _diemHoa,
    _email
){
    this.ma= _maSV;
    this.ten = _tenSV;
    this.email = _email;
    this.matKhau = _matKhau;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.tinhDTB = function(){
        return (this.diemToan + this.diemHoa + this.diemLy) / 3;
    };
}